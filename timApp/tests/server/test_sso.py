from flask import session

from timApp.auth.login import MissingUserInfoError
from timApp.auth.login import find_or_create_user_from_social_login
from timApp.user.sso_useraccount import SSOUserAccount
from timApp.tests.server.timroutetest import TimRouteTest
from timApp.timdb.sqa import db
from timApp.user.sso_useraccount import SSOAccountInfo
from timApp.user.user import User, UserOrigin
from timApp.user.usergroup import UserGroup

from timApp.auth.login import WARN_ABOUT_MISSING_INFO
from timApp.auth.login import ASK_FOR_MISSING_INFO

class TestSSO(TimRouteTest):

    def setUp(self):
        super().setUp()
        self.logout()

    """def create_user_google_sign_in(self, id_info):
        user = find_or_create_user_from_google_sign_in(id_info)
        db.session.commit()
        session['user_id'] = user.id"""

    def test_create_user_from_google(self):
        id_info = {'email': 'akuankka@example.com',
                   'name': 'Aku Ankka',
                   'sub': '1946293345362233',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )
        self.assertEqual('Aku Ankka', self.current_user.real_name)
        self.assertEqual('Google:akuankka@example.com', self.current_user.name)
        self.assertEqual('akuankka@example.com', self.current_user.email)

    def test_login_with_missing_email(self):
        id_info = {'name': 'Aku Ankka',
                   'sub': '224487436514651125499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}

        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400,
                       expect_content={'error': WARN_ABOUT_MISSING_INFO + ' Missing email.' + ASK_FOR_MISSING_INFO}
                       )
    '''
    def test_login_with_missing_is_test(self):
        id_info = {'name': 'Aku Ankka',
                   'email': 'akuankka@example.com',
                   'sub': '224487436514651125499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}

        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False},
                       expect_status=400,
                       expect_content={'error': 'Invalid token'}
                       )
    '''

    def test_login_with_missing_name_and_email(self):
        id_info = {'sub': '224487436514651125499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}

        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400,
                       expect_content={'error': WARN_ABOUT_MISSING_INFO
                                                + ' Missing email. Missing full name.' + ASK_FOR_MISSING_INFO}
                       )

    def test_login_with_missing_name(self):
        id_info = {'email': 'akuankka@example.com',
                   'sub': '224487436514651125499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}

        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400,
                       expect_content={'error': WARN_ABOUT_MISSING_INFO + ' Missing full name.' + ASK_FOR_MISSING_INFO}
                       )

    def test_login_with_new_user(self):
        id_info = {'name': 'Mikki Hiiri',
                   'email': 'mikkihiiri@example.com',
                   'sub': '224487436514651125499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}

        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )
        self.assertEqual('mikkihiiri@example.com', self.current_user.email)
        self.assertEqual('Mikki Hiiri', self.current_user.real_name)
        self.assertEqual('Google:mikkihiiri@example.com', self.current_user.name)

    def test_new_user_with_duplicate_email_1(self):
        """
        Just creates a user.
        :return:
        """
        id_info = {'name': 'Augustus Caesar',
                   'email': 'ac@example.com',
                   'sub': '444487436514651125455',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )

    def test_new_user_with_duplicate_email_2(self):
        """
        If case test_login_with_duplicate_email_1 does not
        succeed this is meaningless.
        :return:
        """
        id_info = {'name': 'Caesar',
                   'email': 'ac@example.com',
                   'sub': '444487436514651125466',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400
                       )

    def test_login_as_existing_user(self):
        """
        Dependant on test_new_user_with_duplicate_email_1,
        uses the same origin id (sub) as the user in that
        test.
        :return:
        """
        id_info = {'name': 'Augustus Caesar',
                   'email': 'ac@example.com',
                   'sub': '444487436514651125455',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )
        self.assertEquals('Augustus Caesar', self.current_user.real_name)
        self.assertEquals('ac@example.com', self.current_user.email)
        self.assertEquals('Google:ac@example.com', self.current_user.name)

    def test_login_as_existing_user_with_changed_email_and_name(self):
        """
        Dependant on test_new_user_with_duplicate_email_1,
        uses the same origin id (sub) as the user in that
        test.
        :return:
        """
        id_info = {'name': 'Augustus Caesar Superbus',
                   'email': 'augustus@example.com',
                   'sub': '444487436514651125455',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )
        self.assertEquals('Augustus Caesar Superbus', self.current_user.real_name)
        self.assertEquals('augustus@example.com', self.current_user.email)
        self.assertEquals('Google:augustus@example.com', self.current_user.name)

        sso_useraccount: SSOUserAccount = self.current_user.sso_useraccounts[0]

        self.assertEqual('444487436514651125455', sso_useraccount.origin_id)

    def test_login_as_multiple_users(self):
        id_info = {'name': 'Konstantinus',
                   'email': 'konstantinus@example.com',
                   'sub': '444477736514651125455',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        id_info2 = {'name': 'Vespasianus',
                   'email': 'vespasianus@example.com',
                   'sub': '224487436514688825499',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=200
                       )
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info2, 'add_user': True, 'is_test': True},
                       expect_status=200
                       )
        self.assertEqual('konstantinus@example.com', self.current_user.email)
        other_users: dict = session.get('other_users')
        for value in other_users.values():
            self.assertEqual('vespasianus@example.com', value['email'])

    def test_new_user_empty_email(self):
        id_info = {'name': 'Nerva',
                   'email': '',
                   'sub': '444487436514651125777',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400
                       )

    def test_new_user_empty_name(self):
        id_info = {'name': '',
                   'email': 'caligula@example.com',
                   'sub': '444487436888651125777',
                   'extra_param1': {1: ['test', 'test2'], 'somekey': 'somevalue'},
                   'extra_param2': 2.0}
        self.json_post('googleSignIn',
                       json_data={'id_token': id_info, 'add_user': False, 'is_test': True},
                       expect_status=400
                       )