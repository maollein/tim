from timApp.tests.db.timdbtest import TimDbTest
from timApp.user.user import User, UserInfo


class SocialLoginTests(TimDbTest):

    def test_create_user(self):
        ui = UserInfo(email='testi@testi.testi', full_name='Testi Testi', username='Testi')
        u, _ = User.create_with_group(ui)
        self.assertEqual(u.real_name, 'Testi Testi')