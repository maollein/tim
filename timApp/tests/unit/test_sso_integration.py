from unittest import TestCase
from timApp.auth.login import get_user_info_from_google_id_token, MissingUserInfoError


class TestSSOIntegration(TestCase):

    def test_all_info_exists(self):
        id_info = {'email': 'testuser1@example.com', 'name': 'Test User1',
                   'given_name':'Test', 'family_name':'User1', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, 'Google:testuser1@example.com', 'Wrong username!')
        self.assertEqual(real_name, 'Test User1', 'Wrong real_name!')
        self.assertEqual(email, 'testuser1@example.com', 'Wrong email!')
        self.assertEqual(given_name, 'Test', 'Wrong given name!')
        self.assertEqual(last_name, 'User1', 'Wrong last name!')

    def test_missing_given_and_last_names(self):
        id_info = {'email': 'testuser1@example.com', 'name': 'Test User1', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, 'Google:testuser1@example.com', 'Wrong username!')
        self.assertEqual(real_name, 'Test User1', 'Wrong real_name!')
        self.assertEqual(email, 'testuser1@example.com', 'Wrong email!')
        self.assertEqual(given_name, None, 'Wrong given name!')
        self.assertEqual(last_name, None, 'Wrong last name!')

    def test_missing_email(self):
        id_info = {'email': None, 'name': 'Aku Ankka', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, None, 'Username should be none')
        self.assertEqual(real_name, 'Aku Ankka', 'real_name should be Aku Ankka')
        self.assertEqual(email, None, 'Email should be none')

    def test_missing_name(self):
        id_info = {'email': 'testuser1@example.com', 'name': '', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, 'Google:testuser1@example.com', 'Wrong username!')
        self.assertEqual(real_name, None, 'Wrong real_name!')
        self.assertEqual(email, 'testuser1@example.com', 'Wrong email!')

    def test_complete_missing_email(self):
        id_info = {'name': '', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, None)
        self.assertEqual(real_name, None)
        self.assertEqual(email, None)

    def test_complete_missing_name(self):
        id_info = {'email': 'testuser1@example.com', 'testi': 'testi', 'testi2': [1, 2, 3, 4]}
        username, real_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        self.assertEqual(username, 'Google:testuser1@example.com')
        self.assertEqual(real_name, None)
        self.assertEqual(email, 'testuser1@example.com')


