from dataclasses import dataclass
from timApp.timdb.sqa import db
from sqlalchemy.dialects.postgresql import ENUM
from typing import Optional
from timApp.user.user import UserOrigin


@dataclass
class SSOAccountInfo:
    username: str
    useraccount_id: Optional[int] = None
    given_name: Optional[str] = None
    last_name: Optional[str] = None
    full_name: Optional[str] = None
    email: Optional[str] = None
    origin_id: Optional[str] = None
    origin: Optional[UserOrigin] = None


class SSOUserAccount(db.Model):


    __tablename__ = 'sso_useraccount'

    id = db.Column(db.Integer, primary_key=True)
    useraccount_id = db.Column(db.Integer, db.ForeignKey('useraccount.id', ondelete='CASCADE'), nullable=False)
    """The id of the useraccount which this sso_useraccount belongs to."""

    name = db.Column(db.Text, nullable=False, unique=True)
    """User name (not full name)."""

    given_name = db.Column(db.Text)
    last_name = db.Column(db.Text)

    real_name = db.Column(db.Text)
    """Real (full) name. This may be in the form "Lastname Firstname" or "Firstname Lastname"."""

    email = db.Column(db.Text)
    """Email address."""

    origin_id = db.Column(db.Text, nullable=False)
    origin = db.Column(ENUM('Email', 'Korppi', 'Sisu', 'Haka', 'OpenID', 'OpenIDConnect',
                            'Facebook', 'Google', 'Twitter', name='userorigin', create_type=False), nullable=False)

    primary_to_user = db.Column(db.Integer, db.ForeignKey('useraccount.id'), nullable=True)
    """Represents the information that this row has been chosen as the primary 
       login account for the useraccount for which this sso_useraccount belongs.
       parent_id can only be empty or the same as useraccount_id."""

    user = db.relationship('User', back_populates='sso_useraccounts', lazy='joined',
                           innerjoin=True, foreign_keys=[useraccount_id])

    primary_to = db.relationship("User", back_populates="primary_account", foreign_keys=[primary_to_user])

    __table_args__ = (db.UniqueConstraint('origin_id', 'origin', name='sso_account_unique'),
                      db.UniqueConstraint('email', 'origin', name='email_origin_unique'),)


    @staticmethod
    def get_by_useraccount_id_and_origin(origin: UserOrigin, origin_id: str) -> Optional['SSOUserAccount']:
        return SSOUserAccount.query.filter_by(origin=origin.name, origin_id=origin_id).first()


    @staticmethod
    def create(info: SSOAccountInfo, set_primary: bool = False) -> 'SSOUserAccount':
        """Creates a new sso_useraccount.

        :param info SSO user account information
        :param set_primary Determines if sso_useraccount is set as primary
        :returns: The newly created sso_useraccount.

        """

        primary = info.useraccount_id if set_primary else None
        sso_useraccount = SSOUserAccount(
            useraccount_id=info.useraccount_id,
            name=info.username,
            given_name=info.given_name,
            last_name=info.last_name,
            real_name=info.full_name,
            email=info.email,
            origin_id=info.origin_id,
            origin=info.origin.name,
            primary_to_user=primary
        )
        db.session.add(sso_useraccount)
        db.session.flush()
        return sso_useraccount

    def update_info(
            self,
            info: SSOAccountInfo,
    ):
        self.name = info.username
        if info.given_name and info.last_name:
            self.given_name = info.given_name
            self.last_name = info.last_name
        self.real_name = info.full_name
        self.email = info.email

