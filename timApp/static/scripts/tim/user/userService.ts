import {saveCurrentScreenPar} from "../document/parhelpers";
import {showMessageDialog} from "../ui/dialog";
import {genericglobals} from "../util/globals";
import {$http, $httpParamSerializer} from "../util/ngimport";
import {to, ToReturn} from "../util/utils";
import {ADMIN_GROUPNAME, IFullUser, IUser} from "./IUser";

export interface ILoginResponse {
    other_users: IUser[];
    current_user: IFullUser;
    last_fb_logged_user: string;
    last_google_logged_user: string;
}

export interface IRedirectResponse {
    redirect_url: string;
}

export class UserService {
    private current: IFullUser; // currently logged in user
    private group: IUser[]; // any additional users that have been added in the session - this does not include the main user
    private lastFacebookLoggedUser: string; // The id of the user last logged in with Facebook account
    private lastGoogleLoggedUser: string; // The id of the user last logged in with Google account

    constructor(current: IFullUser, group: IUser[], lastFacebookLoggedUser: string, lastGoogleLoggedUser: string) {
        this.current = current;
        this.group = group;
        this.lastFacebookLoggedUser = lastFacebookLoggedUser;
        this.lastGoogleLoggedUser = lastGoogleLoggedUser;
    }

    public getCurrent(): IFullUser {
        return this.current;
    }

    public getCurrentPersonalFolderPath(): string {
        return this.getCurrent().folder.path;
    }

    public getSessionUsers() {
        return this.group;
    }

    public getLastFacebookLoggedUser(): string {
        return this.lastFacebookLoggedUser;
    }

    public getLastGoogleLoggedUser(): string {
        return this.lastGoogleLoggedUser;
    }

    public async logout(user: IUser, logoutFromKorppi = false) {
        const r = await to($http.post<ILoginResponse>("/logout", {user_id: user.id}));
        if (!r.ok) {
            void showMessageDialog(r.result.data.error);
            return;
        }
        const response = r.result;
        this.group = response.data.other_users;
        this.current = response.data.current_user;
        if (!this.isLoggedIn()) {
            if (logoutFromKorppi) {
                this.korppiLogout(() => {
                    window.location.reload();
                });
            } else {
                window.location.reload();
            }
        }
    }

    public isLoggedIn() {
        return this.current.id > 0; // TODO: maybe !== 0
    }

    public korppiLogin(addUser: boolean) {
        saveCurrentScreenPar();
        const targetUrl = "/openIDLogin?provider=korppi";
        const separator = targetUrl.includes("?") ? "&" : "?";
        const cameFromRaw = "";
        const anchorRaw = window.location.hash.replace("#", "");
        const redirectFn = () => {
            window.location.replace(targetUrl + separator + $httpParamSerializer({
                came_from: cameFromRaw,
                anchor: anchorRaw,
                add_user: addUser,
            }));
        };
        if (addUser) {
            this.korppiLogout(redirectFn);
        } else {
            redirectFn();
        }
    }

    /**
     * Checks whether the user belongs to a group.
     */
    public belongsToGroup(groupName: string) {
        for (const group of this.current.groups) {
            if (group.name === groupName) {
                return true;
            }
        }
        return false;
    }

    public isGroupAdmin() {
        return userBelongsToGroupOrIsAdmin("Group admins");
    }

    public korppiLogout(redirectFn: () => void) {
        $http(
            {
                withCredentials: true,
                method: "POST",
                url: "https://korppi.jyu.fi/openid/manage/manage",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                data: $httpParamSerializer({logout: "Logout"}),
            }).finally(redirectFn);
    }

    public async loginWithEmail(email: string, password: string, addUser: boolean): ToReturn<ILoginResponse> {
        const r = await to($http<ILoginResponse>(
            {
                method: "POST",
                url: "/altlogin",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "X-Requested-With": "XMLHttpRequest",
                },
                data: $httpParamSerializer({
                    email,
                    password,
                    add_user: addUser,
                }),
            }));
        if (r.ok) {
            this.group = r.result.data.other_users;
            this.current = r.result.data.current_user;
        }
        return r;
    }

    public async loginWithGoogleSignIn(idToken: string, addUser: boolean): ToReturn<ILoginResponse> {
        const r = await to($http<ILoginResponse>(
            {
                method: "POST",
                url: "/googleSignIn",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                },
                data: {
                    id_token: idToken,
                    add_user: addUser,
                },
            }));
        if (r.ok) {
            this.group = r.result.data.other_users;
            this.current = r.result.data.current_user;
            this.lastGoogleLoggedUser = r.result.data.last_google_logged_user;
        }
        return r;
    }

    public async loginWithFacebookLogin(accessToken: string, addUser: boolean): ToReturn<ILoginResponse> {
        const r = await to($http<ILoginResponse>(
            {
                method: "POST",
                url: "/facebookLogin",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                },
                data: {
                    access_token: accessToken,
                    add_user: addUser,
                },
            }));
        if (r.ok) {
            this.group = r.result.data.other_users;
            this.current = r.result.data.current_user;
            this.lastFacebookLoggedUser = r.result.data.last_fb_logged_user;
        }
        return r;
    }

    public async loginWithTwitter(addUser: boolean): ToReturn<IRedirectResponse> {
        const r = await to($http<IRedirectResponse>(
            {
                method: "POST",
                url: "/twitterLogin",
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                },
                data: {
                    add_user: addUser,
                },
            }));
        return r;
    }
}

export const Users = new UserService(genericglobals().current_user, genericglobals().other_users,
    genericglobals().lastFacebookLoggedUser, genericglobals().lastGoogleLoggedUser);

export function isAdmin() {
    return Users.belongsToGroup(ADMIN_GROUPNAME);
}

/**
 * Checks whether user belongs to a certain group or admins group.
 * @returns {boolean}
 */
export function userBelongsToGroupOrIsAdmin(group: string) {
    return isAdmin() || Users.belongsToGroup(group);
}
