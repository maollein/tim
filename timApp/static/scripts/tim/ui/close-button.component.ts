import {Component} from "@angular/core";

@Component({
    selector: "tim-close-button",
    template: `<i title="Close"
                  class="glyphicon glyphicon-remove"></i>
    `,
    styleUrls: ["./close-button.component.scss"],
})
export class CloseButtonComponent {
}
