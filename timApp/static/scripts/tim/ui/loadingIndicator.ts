import {Component} from "@angular/core";

@Component({
    selector: "tim-loading",
    template: `
<i class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></i>
    `,
})
export class LoadingComponent {

}
