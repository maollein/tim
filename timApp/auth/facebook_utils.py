import requests
from flask import current_app


def is_fb_session_valid(session_info_access_token: str) -> bool:
    fb_api_version = current_app.config['FACEBOOK_API_VERSION']
    app_id = current_app.config['FACEBOOK_APP_ID']
    app_secret = current_app.config['FACEBOOK_APP_SECRET']
    app_access_token_combination = app_id + '|' + app_secret
    url = 'https://graph.facebook.com/' + fb_api_version + '/debug_token'
    query_params = {'input_token': session_info_access_token,
                    'access_token': app_access_token_combination}
    r = requests.get(url, params=query_params)
    response = r.json()
    data = response.get('data', None)
    if data:
        return data.get('is_valid', False)
    else:
        return False
