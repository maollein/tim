"""Routes for login view."""
import hashlib
import hmac
import random
import re
import string
import urllib.parse
import requests
import time
from twython import Twython
from twython import TwythonError
from typing import Optional
from typing import Mapping
from typing import Tuple

from dataclasses import dataclass
from flask import Blueprint, render_template
from flask import abort
from flask import current_app
from flask import flash
from flask import redirect
from flask import request
from flask import session
from flask import url_for
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests

from timApp.admin.user_cli import do_merge_users, do_soft_delete
from timApp.auth.accesshelper import verify_admin, AccessDenied
from sqlalchemy.exc import SQLAlchemyError
from timApp.auth.sessioninfo import get_current_user_id, logged_in
from timApp.auth.sessioninfo import get_other_users, get_session_users_ids, get_other_users_as_list, \
    get_current_user_object, get_last_fb_logged_user, get_last_google_logged_user
from timApp.korppi.openid import KorppiOpenIDResponse
from timApp.notification.notify import send_email
from timApp.tim_app import oid, get_home_organization_group
from timApp.timdb.exceptions import TimDbException
from timApp.timdb.sqa import db
from timApp.user.newuser import NewUser
from timApp.user.personaluniquecode import PersonalUniqueCode
from timApp.user.user import User, UserOrigin, UserInfo
from timApp.user.sso_useraccount import SSOUserAccount, SSOAccountInfo
from timApp.user.usergroup import UserGroup
from timApp.user.users import create_anonymous_user
from timApp.user.userutils import create_password_hash, check_password_hash
from timApp.util.flask.requesthelper import verify_json_params, get_option, is_xhr, use_model
from timApp.util.flask.responsehelper import safe_redirect, json_response, ok_response, error_generic
from timApp.util.logger import log_error, log_warning

login_page = Blueprint('login_page',
                       __name__,
                       url_prefix='')  # TODO: Better URL prefix.


GOOGLE_ACCOUNT_PREFIX = 'Google:'
FACEBOOK_ACCOUNT_PREFIX = 'Facebook:'
TWITTER_ACCOUNT_PREFIX = 'Twitter:'
WARN_ABOUT_MISSING_INFO = 'We can\'t get required information from your SSO-profile.'
ASK_FOR_MISSING_INFO = ' To log in to TIM you need to add missing information to your SSO' \
                       ' service and/or give us the permission to get it.'
LOGGED_IN_FB_NOT_TIM = 'You are logged in Facebook but not in TIM!'
POSSIBLY_TWITTER_LOGGED = 'You could not be logged in TIM but you may be logged in Twitter!'


def get_real_name(email):
    atindex = email.index('@')
    if atindex <= 0:
        return email
    parts = email[0:atindex].split('.')
    parts2 = [part.capitalize() if len(part) > 1 else part.capitalize() + '.' for part in parts]
    return ' '.join(parts2)


def is_valid_email(email):
    return re.match('^[\w.-]+@([\w-]+\.)+[\w-]+$', email) is not None


@login_page.route("/logout", methods=['POST'])
def logout():
    user_id, = verify_json_params('user_id', require=False)
    if user_id is not None and user_id != get_current_user_id():
        group = get_other_users()
        group.pop(str(user_id), None)
        session['other_users'] = group
        fb_user_expirations = session.get('fb_user_expirations', dict())
        fb_user_expirations.pop(str(user_id), None)
        session['fb_user_expirations'] = fb_user_expirations
    else:
        session.clear()
    return login_response()


def login_response():
    return json_response(dict(current_user=get_current_user_object().to_json(full=True),
                              other_users=get_other_users_as_list(),
                              last_fb_logged_user=get_last_fb_logged_user(),
                              last_google_logged_user=get_last_google_logged_user()))


@login_page.route('/googleSignIn', methods=['POST'])
def google_sign_in():
    token, add_user, = verify_json_params('id_token', 'add_user')
    is_test, = verify_json_params('is_test', require=False, default=False)
    if not logged_in() and add_user:
        return abort(403, 'You must be logged in before adding users to session.')
    if not add_user and logged_in():
        return abort(400, 'You are already logged in.')
    if session.get('adding_user') is None:
        session['adding_user'] = add_user

    if current_app.config['TESTING'] and is_test:
        # Request is deliberate test. Will skip token validation, token should be dict containing user info.
        username, full_name, email, given_name, last_name = get_user_info_from_google_id_token(token)
        check_user_info(email, username, full_name, given_name, last_name)
        sso_account_info = SSOAccountInfo(username=username, full_name=full_name, email=email,
                                          given_name=given_name, last_name=last_name, origin_id=token['sub'],
                                          origin=UserOrigin.Google)
        user = find_or_create_user_from_social_login(sso_account_info)
        remove_haka_groups(user)
        db.session.commit()
        set_user_to_session(user)
        return finish_login()
    else:
        id_info = validate_google_sign_in_token(token)
        if id_info is None:
            return abort(400, 'Invalid token')

        username, full_name, email, given_name, last_name = get_user_info_from_google_id_token(id_info)
        check_user_info(email, username, full_name, given_name, last_name)
        sso_account_info = SSOAccountInfo(username=username, full_name=full_name, email=email,
                                          given_name=given_name, last_name=last_name, origin_id=id_info['sub'],
                                          origin=UserOrigin.Google)
        user = find_or_create_user_from_social_login(sso_account_info)
        remove_haka_groups(user)
        db.session.commit()
        set_last_google_logged_user(user)
        set_user_to_session(user)
        return finish_login()


def validate_google_sign_in_token(token: str) -> Optional['Mapping[str, any]']:
    try:
        req = google_requests.Request()  # google.auth.transport -requests, not flask request or requests-library
        idinfo = id_token.verify_oauth2_token(token, req, current_app.config['GOOGLE_APP_ID'])
        if idinfo['iss'] not in ['accounts.google.com',
                                 'https://accounts.google.com']:  # issuer must be one or the other
            raise ValueError('Wrong issuer.')
        return idinfo
    except ValueError:
        return None


def get_user_info_from_google_id_token(id_info: Mapping[str, any]) \
        -> Tuple[Optional[str], Optional[str], Optional[str], Optional[str], Optional[str]]:
    """
    Gets and constructs user info for updating or creating user
    from user info mapping gotten from google sign in.
    :param id_info: User info from Google.
    :return: Tuple in this order: username, full_name, email, given_name, last_name.
    """
    email = id_info.get('email')
    full_name = id_info.get('name')
    given_name = id_info.get('given_name')
    last_name = id_info.get('family_name')
    email_verified = id_info.get('email_verified', False)  # Is email verified to be controlled by the end-user
    if not email or not email_verified:
        email = None
        username = None
    else:
        username = GOOGLE_ACCOUNT_PREFIX + email
    if not full_name:
        full_name = None
    return username, full_name, email, given_name, last_name


@dataclass
class FacebookLoginModel:
    access_token: str
    add_user: Optional[bool] = None
    is_test: Optional[bool] = False

@login_page.route('/facebookLogin', methods=['POST'])
@use_model(FacebookLoginModel)
def login_with_facebook_login(m: FacebookLoginModel):
    if not logged_in() and m.add_user:
        return abort(403, 'You must be logged in before adding users to session.')
    if not m.add_user and logged_in():
        return abort(400, 'You are already logged in.')
    if session.get('adding_user') is None:
        session['adding_user'] = m.add_user
    if current_app.config['TESTING'] and m.is_test:
        # Request is deliberate test.
        """
        user = find_or_create_user_from_google_sign_in(m.access_token)
        db.session.commit()
        set_user_to_session(user)
        return finish_login()
        """
    else:
        fb_user_id = validate_facebook_access_token(m.access_token)  # Throws exception if not valid

        email, username, full_name, first_name, last_name \
            = get_user_info_from_facebook(fb_user_id, m.access_token)
        check_user_info(email, username, full_name, first_name, last_name)  # Throws exception if invalid info
        sso_account_info = SSOAccountInfo(email=email, username=username,
                             full_name=full_name, given_name=first_name,
                             last_name=last_name, origin_id=fb_user_id, origin=UserOrigin.Facebook)
        lt_access_token = get_long_term_fb_access_token(m.access_token)
        si_access_token = get_session_info_access_token(lt_access_token)
        user = find_or_create_user_from_social_login(sso_account_info)
        remove_haka_groups(user)
        db.session.commit()
        set_facebook_user_expiration(user, si_access_token)
        set_last_fb_logged_user(user)
        set_user_to_session(user)
        return finish_login()


def validate_facebook_access_token(user_access_token: str) -> str:
    """
    Checks that access token is valid
    :param user_access_token: Access token from facebook
    :return: user_id if access token is valid, otherwise None
    """
    fb_api_version = current_app.config['FACEBOOK_API_VERSION']
    app_id = current_app.config['FACEBOOK_APP_ID']
    app_secret = current_app.config['FACEBOOK_APP_SECRET']
    app_access_token_combination = app_id + '|' + app_secret
    query_params = {'input_token': user_access_token,
                    'access_token': app_access_token_combination}
    r = requests.get('https://graph.facebook.com/' + fb_api_version + '/debug_token', params=query_params)
    if r.status_code == 200:
        user_info = r.json().get('data')
        user_id = user_info.get('user_id')
        is_valid = user_info.get('is_valid')
        app_id_from_access_token = user_info.get('app_id')
        expires_at = user_info.get('expires_at')
        current_time = time.time()
        if is_valid and app_id_from_access_token == app_id and expires_at > current_time and user_id:
            return user_id
        else:
            raise InvalidTokenException('Invalid access token.')
    else:
        raise ExternalAPIError('Something went wrong.')


def get_long_term_fb_access_token(access_token: str) -> str:
    """
    Long lived access token has to be created for us
    to be able to get session info access token
    for the user.
    :param access_token:
    :return:
    """
    fb_api_version = current_app.config['FACEBOOK_API_VERSION']
    app_id = current_app.config['FACEBOOK_APP_ID']
    app_secret = current_app.config['FACEBOOK_APP_SECRET']
    url = 'https://graph.facebook.com/' + fb_api_version + '/oauth/access_token'
    query_params = {'grant_type': 'fb_exchange_token',
                    'client_id': app_id,
                    'client_secret': app_secret,
                    'fb_exchange_token': access_token}
    r = requests.get(url, params=query_params)
    if r.status_code == 200:
        long_lived_access_token = r.json().get('access_token')
        return long_lived_access_token
    else:
        raise ExternalAPIError('Something went wrong. ' + LOGGED_IN_FB_NOT_TIM)


def get_session_info_access_token(long_lived_access_token: str) -> str:
    """
    Session info access token is used to get the validity status
    of a users's facebook session. It grants very little information
    about the user and is thus safer to store than long lived access token.
    :param long_lived_access_token:
    :return:
    """
    fb_api_version = current_app.config['FACEBOOK_API_VERSION']
    app_id = current_app.config['FACEBOOK_APP_ID']
    url = 'https://graph.facebook.com/' + fb_api_version + '/oauth/access_token'
    query_params = {'grant_type': 'fb_attenuate_token',
                    'client_id': app_id,
                    'fb_exchange_token': long_lived_access_token}
    r = requests.get(url, params=query_params)
    if r.status_code == 200:
        session_info_access_token = r.json().get('access_token')
        return session_info_access_token
    else:
        raise ExternalAPIError('Something went wrong. ' + LOGGED_IN_FB_NOT_TIM)


def get_appsecret_proof(access_token: str) -> str:
    """
    appsecret_proof is a hex string hmac used in Facebook's
    API requests for extra safety. Created with sha 256
    using Facebook app secret as the key and user's access token
    as the message.
    :param access_token:
    :return: str
    """
    key: str = current_app.config['FACEBOOK_APP_SECRET']
    key_as_bytes = key.encode()
    access_token_as_bytes = access_token.encode()
    appsecret_proof = hmac.new(key_as_bytes,
                                     msg=access_token_as_bytes,
                                     digestmod=hashlib.sha256).hexdigest()
    return appsecret_proof


def get_user_info_from_facebook(fb_user_id: str, access_token: str) \
        -> Tuple[Optional[str], Optional[str], Optional[str], Optional[str], Optional[str]]:
    fb_api_version = current_app.config['FACEBOOK_API_VERSION']
    url = 'https://graph.facebook.com/' + fb_api_version + '/' + fb_user_id
    query_params = {'fields': 'email,name,first_name,last_name',
                    'access_token': access_token,
                    'appsecret_proof': get_appsecret_proof(access_token)}
    r = requests.get(url, params=query_params)
    if r.status_code == 200:
        user_info = r.json()
        email = user_info.get('email', '')
        full_name = user_info.get('name', '')
        first_name = user_info.get('first_name', '')
        last_name = user_info.get('last_name', '')
        if not email:
            username = ''
        else:
            username = FACEBOOK_ACCOUNT_PREFIX + email
        return email, username, full_name, first_name, last_name
    else:
        raise ExternalAPIError('Something went wrong.')


@dataclass
class TwitterLoginModel:
    add_user: Optional[bool] = None


@login_page.route('/twitterLogin', methods=['POST'])
@use_model(TwitterLoginModel)
def login_with_twitter(m: TwitterLoginModel):
    if not logged_in() and m.add_user:
        return abort(403, 'You must be logged in before adding users to session.')
    if not m.add_user and logged_in():
        return abort(400, 'You are already logged in.')
    if session.get('adding_user') is None:
        session['adding_user'] = m.add_user

    api_key = current_app.config['TWITTER_API_KEY']
    api_secret_key = current_app.config['TWITTER_API_SECRET_KEY']
    twitter_redirect_url = current_app.config['TWITTER_CALLBACK_URL']
    twitter = Twython(app_key=api_key, app_secret=api_secret_key, auth_endpoint='authenticate')
    try:
            auth = twitter.get_authentication_tokens(callback_url=twitter_redirect_url)
    except:
        raise ExternalAPIError('Something went wrong.')
    session['oauth_token'] = auth['oauth_token']
    session['oauth_token_secret'] = auth['oauth_token_secret']
    redirect_url = auth['auth_url']
    return json_response({'redirect_url': redirect_url})


@login_page.route('/twitterLogin/callback', methods=['GET'])
def after_login_with_twitter():
    api_key = current_app.config['TWITTER_API_KEY']
    api_secret_key = current_app.config['TWITTER_API_SECRET_KEY']
    oauth_token = session.pop('oauth_token')
    oauth_token_secret = session.pop('oauth_token_secret')
    oauth_verifier = request.args.get('oauth_verifier')
    oauth_t = request.args.get('oauth_token')

    if oauth_token != oauth_t:
        raise InvalidTokenException('Invalid token')

    twitter = Twython(app_key=api_key, app_secret=api_secret_key, oauth_token=oauth_token,
                      oauth_token_secret=oauth_token_secret)

    try:
        oauth_tokens = twitter.get_authorized_tokens(oauth_verifier)
    except TwythonError:
        raise ExternalAPIError('Something went wrong.')

    origin_id, username, full_name, email = get_twitter_user_info(oauth_tokens)
    check_user_info(email=email, username=username, full_name=full_name)

    user_info = SSOAccountInfo(email=email, username=username, full_name=full_name,
                               origin_id=origin_id, origin=UserOrigin.Twitter)
    user = find_or_create_user_from_social_login(user_info)
    remove_haka_groups(user)
    db.session.commit()
    set_user_to_session(user)
    # In development finish_login does not work without
    # considerable hassle, because url_for does not
    # understand we are using https and non-standard port.
    # We can just comment finish_login and use redirect
    # with the full correct url.
    # return redirect('https://127.0.0.1:3001')
    return finish_login()


def get_twitter_user_info(oauth_tokens: dict) -> Tuple[str, Optional[str], Optional[str], Optional[str]]:
    api_key = current_app.config['TWITTER_API_KEY']
    api_secret_key = current_app.config['TWITTER_API_SECRET_KEY']
    oauth_token = oauth_tokens['oauth_token']
    oauth_token_secret = oauth_tokens['oauth_token_secret']
    twitter = Twython(api_key, api_secret_key, oauth_token, oauth_token_secret)
    try:
        user = twitter.verify_credentials(include_email=True)
    except TwythonError:
        raise ExternalAPIError('Something went wrong. ' + POSSIBLY_TWITTER_LOGGED)
    id = user.get('id_str')
    full_name = user.get('name')
    email = user.get('email')
    if not id:
        raise ExternalAPIError('Something went wrong. ' + POSSIBLY_TWITTER_LOGGED)
    if not email:
        email = None
        username = None
    else:
        username = TWITTER_ACCOUNT_PREFIX + email
    if not full_name:
        full_name = None
    return id, username, full_name, email

def check_user_info(email, username, full_name, first_name=None, last_name=None):
    """
    Checks that there is required user info. At this moment
    aborts login process only if email or full_name
    is missing.
    :param email:
    :param username:
    :param full_name:
    :param first_name:
    :param last_name:
    :return: Nothing
    """
    missing_info = ''
    if not email:
        missing_info += ' Missing email.'
    if not full_name:
        missing_info += ' Missing full name.'
    if missing_info:
        raise MissingUserInfoError(WARN_ABOUT_MISSING_INFO + missing_info + ASK_FOR_MISSING_INFO)
    return


def find_or_create_user_from_social_login(sso_user_info: SSOAccountInfo) -> User:
    """
    Tries to find user based on user id gotten from a social login.
    If user is found, updates the information in sso_useraccount table. Useraccount
    information is updated if sso_useraccount is it's primary account.
    If user is not found, creates new user and sso_useraccount.
    :param sso_user_info: Contains sufficient info for creating or updating a user.
    :return: User
    """
    user_info = UserInfo(email=sso_user_info.email, full_name=sso_user_info.full_name,
                         given_name=sso_user_info.given_name, last_name=sso_user_info.last_name,
                         username=sso_user_info.username, origin=sso_user_info.origin)
    sso_useraccount = get_sso_account(sso_user_info.origin, sso_user_info.origin_id)
    if sso_useraccount:
        sso_useraccount.update_info(sso_user_info)
        user = sso_useraccount.user
        if user.primary_account.id == sso_useraccount.id:
            # We update the useraccount info only if the sso_useraccount used
            # to login is set as the primary sso_useraccount for this useraccount.
            user.update_info(user_info)
        return sso_useraccount.user
    else:
        try:
            user = create_user_from_social_login(user_info)
            sso_user_info.useraccount_id = user.id
            create_sso_useraccount(sso_user_info, True)
            return user
        except SQLAlchemyError:
            return abort(500, 'Could not create user.')


def get_sso_account(origin: UserOrigin, origin_id: str) -> Optional[SSOUserAccount]:
    sso_useraccount = SSOUserAccount.get_by_useraccount_id_and_origin(origin, origin_id)
    return sso_useraccount


def create_user_from_social_login(user_info: UserInfo) -> User:
    try:
        existing_user = User.get_by_email(user_info.email)
        if existing_user:
            msg = 'Account with this email already exists. It is created ' \
                  'with some other SSO-service or directly with TIM.'
            raise CouldNotCreateUserError(msg)
        user, _ = User.create_with_group(user_info)
        db.session.flush()
        return user
    except SQLAlchemyError:  # Happens for example when user creation violates unique constrains
        db.session.rollback()
        raise


def create_sso_useraccount(account_info: SSOAccountInfo, set_primary: bool) -> SSOUserAccount:
    try:
        sso_useraccount = SSOUserAccount.create(account_info, set_primary)
        return sso_useraccount
    except SQLAlchemyError as e:
        db.session.rollback()
        raise


def remove_haka_groups(user: User):
    """
    Remove Haka users group and other
    Haka organization specific groups from
    user.
    :param user:
    :return:
    """
    haka_groups = UserGroup.get_organizations()
    haka_groups.append(UserGroup.get_haka_group())
    user_groups = user.groups
    to_remove = []
    for group in user_groups:
        for ug in haka_groups:
            if group.id == ug.id:
                to_remove.append(group)
                break
    for g in to_remove:
        user.groups.remove(g)
    return


@login_page.route("/login")
def login():
    save_came_from()
    if logged_in():
        flash('You are already logged in.')
        return safe_redirect(session.get('came_from', '/'))
    return render_template('loginpage.html',
                           hide_require_text=True,
                           anchor=request.args.get('anchor'))


@login_page.route("/korppiLogin")
def login_with_korppi():
    return safe_redirect('/openIDLogin?provider=korppi')


def create_or_update_user(
        info: UserInfo,
        group_to_add: UserGroup=None,
):
    user = User.get_by_name(info.username)

    if user is None and info.email:
        user = User.get_by_email(info.email)
    if user is None and info.unique_codes:
        for uc in info.unique_codes:
            puc = PersonalUniqueCode.find_by_code(code=uc.code, codetype=uc.codetype, org=uc.org)
            if puc:
                user = puc.user
                break

    if user is not None:
        if user.origin in (UserOrigin.Facebook, UserOrigin.Google, UserOrigin.Twitter):
            raise Exception('User with the same email has already been created '
                            'with some social media account. Account with the same email '
                            'can not be created.')
        if user.email != info.email and info.email:
            ue = user.get_by_email(info.email)
            if ue and user != ue:
                log_warning(f'Merging users during login: {user.name} and {ue.name}')
                do_merge_users(user, ue)
                do_soft_delete(ue)
                db.session.flush()
            elif ue:
                raise Exception(f'Users were the same but still different email: {user.name}')
        user.update_info(info)
    else:
        user, _ = User.create_with_group(info)

    if group_to_add and group_to_add not in user.groups:
        user.groups.append(group_to_add)
    return user


@login_page.route("/openIDLogin")
@oid.loginhandler
def login_with_openid():
    add_user = get_option(request, 'add_user', False)
    if not logged_in() and add_user:
        raise AccessDenied('You must be logged in before adding users to session.')
    if not add_user and logged_in():
        flash("You're already logged in.")
        return finish_login()
    if session.get('adding_user') is None:
        session['adding_user'] = add_user

    provider = get_option(request, 'provider', None)
    if provider != 'korppi':
        return abort(400, 'Unknown OpenID provider. Only korppi is supported so far.')
    save_came_from()
    # see possible fields at http://openid.net/specs/openid-simple-registration-extension-1_0.html
    return oid.try_login(current_app.config['OPENID_IDENTITY_URL'],
                         ask_for_optional=['email', 'fullname', 'firstname', 'lastname'])


username_parse_error = 'Could not parse username from OpenID response.'


class KorppiEmailException(Exception):
    code = 400
    description = ""


@login_page.errorhandler(KorppiEmailException)
def already_exists(error: KorppiEmailException):
    return error_generic(error.description, 400, template='korppi_email_error.html')


@oid.after_login
def openid_success_handler(resp: KorppiOpenIDResponse):
    m = re.fullmatch('https://korppi.jyu.fi/openid/account/([a-z]+)', resp.identity_url)
    if not m:
        return abort(400, username_parse_error)
    username = m.group(1)
    if not username:
        return abort(400, username_parse_error)
    if not resp.email:
        # Allow existing users to log in even if Korppi didn't give email.
        u = User.get_by_name(username)
        if u:
            log_warning(f'Existing user did not have email in Korppi: {username}')
            set_user_to_session(u)
            return finish_login()
        log_warning(f'New user did not have email in Korppi: {username}')
        raise KorppiEmailException()
    if not resp.fullname:
        return abort(400, 'Missing fullname')
    if not resp.firstname:
        return abort(400, 'Missing firstname')
    if not resp.lastname:
        return abort(400, 'Missing lastname')
    fullname = f'{resp.lastname} {resp.firstname}'
    user = create_or_update_user(
        UserInfo(email=resp.email, full_name=fullname, username=username, origin=UserOrigin.Korppi),
        group_to_add=get_home_organization_group(),
    )
    db.session.commit()
    set_user_to_session(user)
    return finish_login()


def set_facebook_user_expiration(user: User, session_info_access_token: str):
    fb_users = session.get('fb_user_expirations', dict())
    session_checking_info = {'expiration': str(time.time() + current_app.config['FACEBOOK_SESSION_TIME']),
                             'session_info_access_token': session_info_access_token}
    fb_users[str(user.id)] = session_checking_info
    session['fb_user_expirations'] = fb_users


def set_last_fb_logged_user(user: User):
    session['last_fb_logged_user'] = str(user.id)


def set_last_google_logged_user(user: User):
    session['last_google_logged_user'] = str(user.id)


def set_user_to_session(user: User):
    adding = session.pop('adding_user', None)
    if adding:
        if user.id in get_session_users_ids():
            flash(f'{user.real_name} is already logged in.')
            return
        other_users = session.get('other_users', dict())
        other_users[str(user.id)] = user.to_json()
        session['other_users'] = other_users
    else:
        session['user_id'] = user.id
        session.pop('other_users', None)


"""Sent passwords are stored here when running tests."""
test_pws = []


@login_page.route("/checkTempPass", methods=['POST'])
def check_temp_password():
    """Checks that the temporary password provided by user is correct.
    Sends the real name of the user if the email already exists so that the name field can be prefilled.
    """
    email_or_username, token, = verify_json_params('email', 'token')
    nu = check_temp_pw(email_or_username, token)
    u = User.get_by_email(nu.email)
    if u:
        return json_response({'status': 'name', 'name': u.real_name, 'can_change_name': u.is_email_user})
    else:
        return ok_response()


@dataclass
class AltSignupModel:
    email: str
    url: Optional[str] = None


@login_page.route("/altsignup", methods=['POST'])
@use_model(AltSignupModel)
def alt_signup(m: AltSignupModel):
    email_or_username = m.email
    fail = False
    if not is_valid_email(email_or_username):
        u = User.get_by_name(email_or_username)
        if not u:
            # Don't return immediately; otherwise it is too easy to analyze timing of the route to deduce success.
            fail = True
            email = 'nobody@example.com'
        else:
            email = u.email
    else:
        email = email_or_username

    password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    nu = NewUser.query.get(email)
    password_hash = create_password_hash(password)
    if nu:
        nu.pass_ = password_hash
    else:
        nu = NewUser(email=email, pass_=password_hash)
        db.session.add(nu)

    # Real users should never submit the url parameter.
    # It is meant for catching bots.
    if m.url and not (email_or_username.endswith('.fi') or email_or_username.endswith('@gmail.com')):
        log_warning(f'Bot registration attempt: {email_or_username}, URL: {m.url}')
        fail = True

    if fail:
        # We return ok because we don't want to leak any information about the existence of accounts etc.
        return ok_response()

    db.session.commit()

    session.pop('user_id', None)

    try:
        send_email(email, 'Your new TIM password', f'Your password is {password}')
        if current_app.config['TESTING']:
            test_pws.append(password)
        return ok_response()
    except Exception as e:
        log_error(f'Could not send login email (user: {email}, password: {password}, exception: {str(e)})')
        return abort(400, f'Could not send the email, please try again later. The error was: {str(e)}')


def check_temp_pw(email_or_username: str, oldpass: str) -> NewUser:
    nu = NewUser.query.get(email_or_username)
    if not nu:
        u = User.get_by_name(email_or_username)
        if u:
            nu = NewUser.query.get(u.email)
    if not (nu and nu.check_password(oldpass)):
        return abort(400, 'Wrong temporary password. '
                          'Please re-check your email to see the password.')
    return nu


@login_page.route("/altsignup2", methods=['POST'])
def alt_signup_after():
    email_or_username, confirm, password, real_name, temp_pass = verify_json_params(
        'email',
        'passconfirm',
        'password',
        'realname',
        'token',
    )

    if password != confirm:
        return abort(400, 'Passwords do not match.')

    min_pass_len = current_app.config['MIN_PASSWORD_LENGTH']
    if len(password) < min_pass_len:
        return abort(400, f'A password should contain at least {min_pass_len} characters.')

    save_came_from()

    nu = check_temp_pw(email_or_username, temp_pass)
    email = nu.email
    username = email

    user = User.get_by_email(email)
    if user is not None:
        # User with this email already exists
        user_id = user.id
        u2 = User.get_by_name(username)

        if u2 is not None and u2.id != user_id:
            return abort(400, 'User name already exists. Please try another one.')

        # Use the existing user name; don't replace it with email
        username = user.name
        success_status = 'updated'

        # If the user isn't an email user, don't let them change name
        # (because it has been provided by other system such as Korppi).
        if not user.is_email_user:
            real_name = user.real_name

        user.update_info(UserInfo(username=username, full_name=real_name, email=email, password=password))
    else:
        if User.get_by_name(username) is not None:
            return abort(400, 'User name already exists. Please try another one.')
        success_status = 'registered'
        user, _ = User.create_with_group(
            UserInfo(
                username=username,
                full_name=real_name,
                email=email,
                password=password,
                origin=UserOrigin.Email,
            )
        )
        db.session.flush()
        user_id = user.id

    db.session.delete(nu)
    db.session.commit()

    session['user_id'] = user_id
    return json_response({'status': success_status})


def is_possibly_home_org_account(email_or_username: str):
    return bool(re.fullmatch(
        '[a-z]{2,8}|.+@([a-z]+\.)*' + re.escape(current_app.config['HOME_ORGANIZATION']),
        email_or_username,
    ))


@login_page.route("/altlogin", methods=['POST'])
def alt_login():
    save_came_from()
    email_or_username = request.form['email']
    password = request.form['password']
    session['adding_user'] = request.form.get('add_user', 'false').lower() == 'true'

    user = User.get_by_email_or_username(email_or_username)
    if user is not None:
        old_hash = user.pass_
        if user.check_password(password, allow_old=True, update_if_old=True):
            # Check if the users' group exists
            try:
                user.get_personal_group()
            except TimDbException:
                ug = UserGroup(name=user.name)
                user.groups.append(ug)
                db.session.commit()
            set_user_to_session(user)

            # if password hash was updated, save it
            if old_hash != user.pass_:
                db.session.commit()
            return finish_login()
    else:
        # Protect from timing attacks.
        check_password_hash('', '$2b$12$zXpqPI7SNOWkbmYKb6QK9ePEUe.0pxZRctLybWNE1nxw0/WMiYlPu')

    error_msg = "Email address or password did not match."
    home_org = current_app.config['HOME_ORGANIZATION']
    if is_possibly_home_org_account(email_or_username):
        error_msg += f" You might not have a TIM account. {home_org} members can use the {home_org} login button."
    if is_xhr(request):
        return abort(403, error_msg)
    else:
        flash(error_msg, 'loginmsg')
    return finish_login(ready=False)


def save_came_from():
    came_from = request.args.get('came_from') or request.form.get('came_from')
    if came_from:
        session['came_from'] = came_from
        session['last_doc'] = came_from
    else:
        session['came_from'] = session.get('last_doc', '/')
    session['anchor'] = request.args.get('anchor') or request.form.get('anchor') or ''


def finish_login(ready=True):
    if not ready:
        return safe_redirect(url_for('start_page'))

    anchor = session.get('anchor', '')
    if anchor:
        anchor = "#" + anchor
    came_from = session.get('came_from', '/')
    if not is_xhr(request):
        return safe_redirect(urllib.parse.unquote(came_from) + anchor)
    else:
        return login_response()


@login_page.route("/quickLogin/<username>")
def quick_login(username):
    """A debug helping method for logging in as another user.

    For developer use only.

    """
    verify_admin()
    user = User.get_by_name(username)
    if user is None:
        abort(404, 'User not found.')
    session['user_id'] = user.id
    flash(f"Logged in as: {username}")
    return redirect(url_for('view_page.index_page'))


def log_in_as_anonymous(sess) -> User:
    user_name = 'Anonymous'
    user_real_name = 'Guest'
    user = create_anonymous_user(user_name, user_real_name)
    db.session.flush()
    sess['user_id'] = user.id
    return user


class MissingUserInfoError(Exception):
    def __init__(self, msg: str):
        self.msg = msg


class CouldNotCreateUserError(Exception):
    def __init__(self, msg: str):
        self.msg = msg


class InvalidTokenException(Exception):
    def __init__(self, msg: str):
        self.msg = msg


class ExternalAPIError(Exception):
    def __init__(self, msg: str):
        self.msg = msg


@login_page.errorhandler(MissingUserInfoError)
def handle_missing_user_info(error: MissingUserInfoError):
    return error_generic(error.msg, 400)


@login_page.errorhandler(CouldNotCreateUserError)
def handle_could_not_create_user(error: CouldNotCreateUserError):
    return error_generic(error.msg, 400)


@login_page.errorhandler(InvalidTokenException)
def handle_invalid_token(error: InvalidTokenException):
    return error_generic(error.msg, 400)


@login_page.errorhandler(ExternalAPIError)
def handle_external_api_error(error: ExternalAPIError):
    return error_generic(error.msg, 500)
